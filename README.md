# AppWesomed

> (ES) Diseño y desarrollo de una prueba de concepto (PoC) que demuestre la viabilidad y la aportación de valor adicional de la integración de SNOMED en el desarrollo de la plataforma de atención médica online.

> (EN) Design and development of a proof of concept (PoC) that demonstrates the viability and the contribution of additional value of the SNOMED integration in the development of the online health care platform.

    Tecnologías utilizadas / Used technology
        Angular4 - Primeng
        NodeJS - Express
        MongopDB - Mongose


## Formulario Experiencia de Uso - Use Experience Form 

*   La web AppWesomed ha sido una experiencia frustrante / The AppWesomed website has been a frustrating experience
*   La web AppWesomed ha sido fácil de usar / The AppWesomed website has been easy to
*   La web AppWesomed podría usarla a diario / The AppWesomed website could use it daily
*   La esta web puede ayudar para mi trabajo / This website can help me in my work
*   En esta web tengo que pasar demasiado tiempo corrigiendo cosas / On this website I have to spend too much time correcting things
*   Su edad es / your age is
*   ¿Cuál ha sido su experiencia general con la web? / What has been your general experience with the web?
