﻿require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const jwt = require('_helpers/jwt');
const bodyParser = require('body-parser');

const errorHandler = require('_helpers/error-handler');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use('/wesomed/users', require('./models/users/users.controller'));
app.use('/wesomed/patients', require('./models/patients/patient.controller'));
app.use('/wesomed/historias-clinicas', require('./models/historiaClinica/historia.controller'));
app.use('/wesomed/reporte', require('./models/reporte/reporte.controller'));
app.use('/wesomed/log', require('./models/logs/log.controller'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? 81 : 5000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
