const config = require('config.json');
const mongoose = require('mongoose');

mongoose.connect(config.connectionString);

mongoose.Promise = global.Promise;

module.exports = {
    User: require('../models/users/user.model'),
    Patient: require('../models/patients/patient.model'),
    HistoriaClinica : require('../models/historiaClinica/historia.model'),
    Reporte: require('../models/reporte/reporte.model'),
    Log: require('../models/logs/log.model')
};