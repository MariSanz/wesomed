﻿const db = require('_helpers/db');
const Patient = db.Patient;

module.exports = {
    getByName,
    getAll,
    getByNss,
    getBySurname,
    getByPatients,
    delete: _delete
};

async function getAll() {
    return await Patient.find();
}

async function getByNss(nssParam) {
    return await Patient.find( { "nss" : nssParam });
}

async function getByName(nameParam) {
    return await Patient.find(  { "name" : '/.*'+nameParam+'.*/' });
}

async function getBySurname(surmaneParam) {
    return await Patient.find( { "surname" : '/.*'+surmaneParam+'.*/' } );
}

async function getByPatients(nss, name, surname) {
   
     if( nss != undefined && name != undefined  && surname != undefined  ){ 

        return await Patient.find(  { "nss" : nss, 
                    "name" : name,
                    "surname" : surname });
    }else if( name != undefined  && surname != undefined ) {

        return await Patient.find(  {"name" : name,
                        "surname" : surname });
    } else if( nss != undefined  && name != undefined  ) {

        return await Patient.find(  {"nss" : nss,
            "name" : name });
    } else if( nss != undefined  && surname != undefined ) {

        return await Patient.find(  {"nss" : nss,
            "surname" : surname });
    } else if(  nss != undefined  ) {
	
        return await Patient.find(  {"nss" : nss });
    }  else if(  name != undefined  ) {

        return await Patient.find(  {"name" : name });
    }  else if(  surname != undefined  ) {

        return await Patient.find(  {"surname" : surname });
    }

    return await Patient.find();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}