﻿const express = require('express');
const router = express.Router();
const patientService = require('./patient.service');

// routes
router.get('/', getAll);
router.get('/:nss', getByNss);
router.post('/', getByPatients);
router.get('/:nss/:name/:surname', getByPatients);
router.delete('/:id', _delete);

module.exports = router;

function getByNss(req, res, next) {
    console.log('Paciente '+req.params.nss);
    patientService.getByNss(req.params.nss)
        .then(patient => patient ? res.json(patient) : res.sendStatus(404))
        .catch(err => next(err));
}

function getByNssAndName(req, res, next) {
  
    patientService.getByPatients(req.params.nss, req.params.name, req.params.surname)
        .then(patient => patient ? res.json(patient) : res.sendStatus(404))
        .catch(err => next(err));
}

function getByPatients(req, res, next) {

    patientService.getByPatients(req.body.nss, req.body.name, req.body.surname)
        .then(patient => patient ? res.json(patient) : res.sendStatus(404))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    patientService.getAll()
        .then(patients => res.json(patients))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    patientService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}