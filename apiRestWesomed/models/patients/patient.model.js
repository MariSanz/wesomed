const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    nss: { type: String, unique: true, required: true },
    name: { type: String, required: true },
    firstName: { type: String, required: true },
    surname: { type: String, required: true },
    follow : { type: Boolean, default: false},
    age : { type: Number, default: 0}
  
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Patient', schema);