const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id: mongoose.Schema.Types.ObjectId,
    date: { type: Date, default: Date.now },
    status: { type: String },
    patient: {      
        identifier : { type: String },    
        firstName:{ type: String },
        surName : { type: String },
        gender: {
            type: String,
            enum: ["male", "female"]
        },
        birthdate: { type: Date, default: Date.now },
    } ,
    practitioner: {     
        identifier : { type: String },
        active: { type: Boolean },
        telecom: { type: String },
        firstName: { type: String },
        surName : { type: String },
    },
    result: [ {
        observation :{
            status : { type: String },
            context: { 
            encounter : {
                class :{ type: String },
                diagnosis : [{
                    condition : {
                        coding : { 
                            code : { type: String },
                            display : { type: String },
                            system : { type: String }
                        },
                        snomed_term: { 
                            id: { type: String },
                            snomedid : { type: String },
                            prefLabel : { type: String },
                            associatedWith : [{
                                id: { type: String },
                                snomedid : { type: String },
                                prefLabel :{ type: String },
                            }]
                         }
                    }
                }]
                } 
            },
            status : { type : String }
        }
    } ]
    
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('HistoriaClinica', schema);