﻿const express = require('express');
const router = express.Router();
const historiaService = require('./historia.service');

// routes
router.get('/', getAll);
router.get('/:id', getByPatient);
router.put('/', guardar);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    historiaService.getAll()
        .then(historias => { 
            res.json(historias);
        })
        .catch(err => next(err));
}

function guardar(req, res, next) {  
    historiaService.create(req.body)
        .then(() => {
            res.json({});
        })
        .catch(err => next(err));
}

function getByPatient(req, res, next) {
 
    historiaService.getByPatient(req.params.id)
    .then(historias => historias ? res.json(historias) : res.sendStatus(404))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    historiaService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}