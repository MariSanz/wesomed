﻿const db = require('_helpers/db');
const HistoriaClinica = db.HistoriaClinica;

module.exports = {
    create,
    getAll,
    getByPatient,
    delete: _delete
};

async function getAll() {
    return await HistoriaClinica.find();
}

async function create(historiaParam) {
   
    const historia = new HistoriaClinica(historiaParam);
    // save historia
   return await historia.save();
   
}

async function getByPatient(nss) {

    return await HistoriaClinica.find({
            "patient.identifier": nss
        })
        .catch(function (err) {
            console.log("GetPatient");
            console.log(err);
        });
}

async function getById(id) {
    return await HistoriaClinica.findById(id);
}

async function _delete(id) {
    await HistoriaClinica.findByIdAndRemove(id);
}