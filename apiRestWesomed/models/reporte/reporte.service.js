﻿const db = require('_helpers/db');
const Reporte = db.Reporte;

module.exports = {
    create,
    getAll,
    getByPatient,
    delete: _delete
};

async function getAll() {
    return await Reporte.find();
}

async function getByPatient(idPatient){
  
    return await Reporte.find(  { "patient.identifier": idPatient });
}

async function create(reporteParam) {
    const reporte = new Reporte(reporteParam);
    // save reporte
    await reporte.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}