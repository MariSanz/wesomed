const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id: mongoose.Schema.Types.ObjectId,
    fecha: { type: Date, default: Date.now },
    patient: {      
        identifier : { type: String },    
        firstName:{ type: String },
        surName : { type: String },
        gender: {
            type: String,
            enum: ["male", "female"]
        },
        birthdate: { type: Date, default: Date.now },
    } ,
    anamnesis: { type: String },
    diagnostico: { type: String },
    tratamiento: { type: String }, 
    historyId:  { type: String }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Reporte', schema);