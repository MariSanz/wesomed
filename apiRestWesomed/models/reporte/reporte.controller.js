﻿const express = require('express');
const router = express.Router();
const reporteService = require('./reporte.service');

// routes

router.get('/', getAll);
router.get('/:idPatient', getByPatient);
router.put('/', guardar);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
    reporteService.getAll()
        .then(reporte => res.json(reportes))
        .catch(err => next(err));
}

function getByPatient(req, res, next) {
   
    reporteService.getByPatient(req.params.idPatient)
        .then(reporte => res.json(reporte))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    reporteService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function guardar(req, res, next) {  
    reporteService.create(req.body)
        .then(() => {
            res.json({});
        })
        .catch(err => next(err));
}