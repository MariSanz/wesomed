const db = require('_helpers/db');
const Log = db.Log;

module.exports = {
    create,
    getAll, 
    update
};

async function getAll() {
    return await Log.find();
}

async function create(logParam) {

    const log = new Log(logParam);

    // save historia
    await log.save();
}

async function update(username, logParam) {
    const log = await Log.findByUser(username)

    // validate
    if (!log) throw 'Log not found';
    if (log.user !== logParam.user && await Log.findOne({ user: logParam.user })) {
        throw 'Username of Log "' + logParam.user + '" is already taken';
    }

    // copy userParam properties to user
    Object.assign(log, logParam);

    await log.save();
}