const express = require('express');
const router = express.Router();
const logService = require('./log.service');

// routes
router.get('/', getAll);
router.post('/', guardar);
router.put('/', update);

module.exports = router;

function getAll(req, res, next) {
    logService.getAll()
        .then(logs => res.json(logs))
        .catch(err => next(err));
}

function guardar(req, res, next) {  
	
    logService.create(req.body)
        .then(() => res.json({}))
        .catch(err => {
            next(err);
        });
}

function update(req, res, next) {
    logService.update(req.body.user, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}