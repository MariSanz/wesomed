const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    id: mongoose.Schema.Types.ObjectId,
   
    user: { type: String, required: true },
    message:  {
        text:{ type: String, required: false },
        level: { type: String, required: true },
        source : { type: String, required: true},
        date: { type: Date, required: true }
    },
   
    
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Log', schema);
