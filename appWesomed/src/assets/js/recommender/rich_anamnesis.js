/* 
 * Date 18/05/2017
 */
var textAnamnesis = $('[data-id=anamnesis]')[0];
var countCharAnamnesis = 0;
var bufferAnamnesis = "";
var bufferItemAnamnesis = [];


       
        var json = {
            "people": {
                "person": [{
                    "name": "Peter",
                    "age": 43,
                    "sex": "male"},
                {
                    "name": "Zara",
                    "age": 40,
                    "sex": "female"},
                {
                    "name": "Ana",
                    "age": 65,
                    "sex": "female"},
                {
                    "name": "Anabel",
                    "age": 65,
                    "sex": "female"},
                {
                    "name": "Alicia",
                    "age": 22,
                    "sex": "female"},
                {
                    "name": "Aurora",
                    "age": 32,
                    "sex": "female"}]
            }
        };

function focusAnamnesis(){
    var nodePlaceholder = $('#placeholder-anamnesis');  
    if(nodePlaceholder.text().length !== 0){
        nodePlaceholder.remove();
    }
};
function blurAnamnesis(){
    if(this.innerText.length === 0){
        var p = document.createElement("p"); 
        p.setAttribute("class", "placeholder");
        p.setAttribute("id","placeholder-anamnesis");
        
        p.textContent = "Introduzca los síntomas evaluados...";
        this.append(p);
    }
};

function keyupAnamnesis() {  
  
     if(event.keyCode === 32){                        
        bufferAnamnesis = "";
        countCharAnamnesis = 0;
    }else if(event.keyCode === 8){                        
        bufferAnamnesis = bufferAnamnesis.slice(0, -1);
        countCharAnamnesis--;
        $('ul[data-box-type="anamnesis"]').remove();
    }else{
        var entrada = String.fromCharCode(event.keyCode);
        if (/[a-zA-Z]/.test(entrada)){
        
            bufferAnamnesis+=event.key;
            countCharAnamnesis++;

            socket.emit('new-anamnesis', bufferAnamnesis);

            socket.on('result-anamnesis', function(dato){
               
            });

        
            $('ul[data-box-type="anamnesis"]').remove();


            $.each(json.people.person, function(i, v) {

                if (v.name.search(new RegExp('^'+bufferAnamnesis, 'i'), '') !== -1) {

                    bufferItemAnamnesis.push(v.name);
                    return;
                }
            });                

            if(bufferItemAnamnesis.length !== 0 ){
                var contenedor = document.createElement("ul");
                contenedor.className  = "box-tags";
                contenedor.setAttribute('data-box-type', 'anamnesis');

                bufferItemAnamnesis.forEach(function(element, index) {
                var tag = document.createElement("li");
                    tag.innerHTML = '<span class="badge tags" id="'+index+'"> '+element.toString()+'</span>';                    
                    tag.onclick = function(){
                    $('ul[data-box-type="anamnesis"]').remove();
                    var anamnesis = document.getElementById('anamnesis');
                    var textAnamnesis = $('#anamnesis').html();
                    var textActualizado;
                    var me = this;
                    
                        //borrar caracteres por los que se predice
                        if(textAnamnesis.length>=1){
                            textActualizado = anamnesis.innerHTML.slice(0,-(countCharAnamnesis+('<div contenteditable="true"></div>'.length)));

                        }else{
                            textActualizado = anamnesis.innerHTML.slice(0,-(countCharAnamnesis));

                        }
                        $('#anamnesis').html(textActualizado);
                        //texto con el termino seleccionado
                        var tamano=me.childNodes[0].textContent.length;
                        tamano*=7; //el valor multiplicativo debe cambiarse dependiendo del tamaño de la fuente

                        var input = document.createElement("input"); 
                        input.setAttribute("style", 'color:red; border: none; width:'+tamano+'px;"');
                        input.setAttribute("value", me.childNodes[0].textContent);

                        $('#anamnesis').append(input);

                        var divSiguiente = document.createElement("div");  
                        divSiguiente.setAttribute("contenteditable", "true");
                        $('#anamnesis').append(divSiguiente);

                        var divTratamiento = document.getElementById('anamnesis');
                        let s = window.getSelection();
                        let r = document.createRange();
                        r.setStart(divTratamiento, divTratamiento.childElementCount);
                        r.setEnd(divTratamiento, divTratamiento.childElementCount);
                        s.removeAllRanges();
                        s.addRange(r);
                        
                        countCharAnamnesis = 0;                                
                        bufferAnamnesis = "";


                    };            
                    contenedor.appendChild(tag);
                });

                var anamnesis = document.getElementById('anamnesis');
                anamnesis.appendChild(contenedor);
            }

            bufferItemAnamnesis = []; //reiniciar lista de items
        }
    }
};



