/* 
 * Date 18/05/2017
 */
var textDiagnostico = $('#diagnostico')[0];
var bufferDiagnostico = "";
var countCharDiagnostico = 0;
var bufferDiagnostico = "";
var bufferItemDiagnostico = [];

      var json = {
            "people": {
                "person": [{
                    "name": "Peter",
                    "age": 43,
                    "sex": "male"},
                {
                    "name": "Zara",
                    "age": 40,
                    "sex": "female"},
                {
                    "name": "Ana",
                    "age": 65,
                    "sex": "female"},
                {
                    "name": "Carlos",
                    "age": 65,
                    "sex": "male"},
                {
                    "name": "Carmen",
                    "age": 22,
                    "sex": "female"},
                {
                    "name": "Cecilia",
                    "age": 32,
                    "sex": "female"}]
            }
        };

textDiagnostico.onfocus = function(){
    var nodePlaceholder = $('#placeholder-diagnostico');
   
    if(nodePlaceholder.text().length !== 0){
        nodePlaceholder.remove();
    }
};
textDiagnostico.onblur = function(){
    if(this.innerText.length === 0){
        var p = document.createElement("p"); 
        p.setAttribute("class", "placeholder");
        p.setAttribute("id","placeholder-diagnostico");
       
        p.textContent = "Introduzca el diagnóstico evaluado...";
        this.append(p);
    }
};

textDiagnostico.onkeyup = function(e) {  
   
     if(e.keyCode === 32 ){                        
        bufferDiagnostico = "";
        countCharDiagnostico = 0;
    }else if(e.keyCode === 8){                        
        bufferDiagnostico = bufferDiagnostico.slice(0, -1);
        countCharDiagnostico--;
        $('ul[data-box-type="diagnostico"]').remove();
    }else{
         var entrada = String.fromCharCode(e.keyCode);
         if (/[a-zA-Z]/.test(entrada)){
            bufferDiagnostico+=e.key; 
            countCharDiagnostico++;

            socket.emit('new-diagnostico', bufferDiagnostico);

            socket.on('result-diagnostico', function(dato){
                console.log(dato);
            });
            
            $('ul[data-box-type="diagnostico"]').remove();
        
            $.each(json.people.person, function(i, v) {

                if (v.name.search(new RegExp('^'+bufferDiagnostico, 'i'), '') !== -1) {

                    bufferItemDiagnostico.push(v.name);
                    return;
                }
            });                

            if(bufferItemDiagnostico.length !== 0){
                var contenedor = document.createElement("ul");
                contenedor.className  = "box-tags";
                contenedor.setAttribute('data-box-type', 'diagnostico');

                bufferItemDiagnostico.forEach(function(element, index) {
                var tag = document.createElement("li");
                    tag.innerHTML = '<span class="badge tags" id="'+index+'"> '+element.toString()+'</span>';                    
                    tag.onclick = function(){
                    $('ul[data-box-type="diagnostico"]').remove();
                    var diagnostico = document.getElementById('diagnostico');
                    var textDiagnostico = $('#diagnostico').html();
                    var textActualizado;
                    var me = this;
                    
                        //borrar caracteres por los que se predice                   
                        if(textDiagnostico.length>=1){
                            textActualizado = diagnostico.innerHTML.slice(0,
                                -(countCharDiagnostico+('<div contenteditable="true"></div>'.length)));
                        }else{
                            textActualizado = diagnostico.innerHTML.slice(0,-(countCharDiagnostico));

                        }
                        $('#diagnostico').html(textActualizado);
                        //texto con el termino seleccionado
                        var tamano=me.childNodes[0].textContent.length;
                        tamano*=7; //el valor multiplicativo debe cambiarse dependiendo del tamaño de la fuente

                        var input = document.createElement("input"); 
                        input.setAttribute("style", 'color:red; border: none; width:'+tamano+'px;"');
                        input.setAttribute("value", me.childNodes[0].textContent);

                        /*se añade el tag como un input al div contenteditable*/
                        $('#diagnostico').append(input);
                        
                        var divSiguiente = document.createElement("div");  
                        divSiguiente.setAttribute("contenteditable", "true");
                        $('#diagnostico').append(divSiguiente);
                        
                        var divTratamiento = document.getElementById('diagnostico');
                        let s = window.getSelection();
                        let r = document.createRange();
                        r.setStart(divTratamiento, divTratamiento.childElementCount);
                        r.setEnd(divTratamiento, divTratamiento.childElementCount);
                        s.removeAllRanges();
                        s.addRange(r);

                        countCharDiagnostico = 0;                                
                        bufferDiagnostico = "";


                    };            
                    contenedor.appendChild(tag);
                });
                
                var diagnostico = document.getElementById('diagnostico');
                diagnostico.appendChild(contenedor);
                
            }

            bufferItemDiagnostico = []; //reiniciar lista de items
        }
    }
};

