/* 
 * Date 18/05/2017 
 */
var textTratamiento = $('#tratamiento')[0];
var countCharTratamiento = 0;
var bufferTratamiento = "";
var bufferItemTratamiento = [];
     var json = {
            "people": {
                "person": [{
                    "name": "Peter",
                    "age": 43,
                    "sex": "male"},
                {
                    "name": "Zara",
                    "age": 40,
                    "sex": "female"},
                {
                    "name": "Ana",
                    "age": 65,
                    "sex": "female"},
                {
                    "name": "Anabel",
                    "age": 65,
                    "sex": "female"},
                {
                    "name": "Alicia",
                    "age": 22,
                    "sex": "female"},
                {
                    "name": "Aurora",
                    "age": 32,
                    "sex": "female"}]
            }
        };

/* Manejo del placeholder en div contenteditable, 
 * agregar y remover el placeholder segun gana o pierde el foco y se ha editado
 */
textTratamiento.onfocus = function(){
    var nodePlaceholder = $('#placeholder-tratamiento');   
    if(nodePlaceholder.text().length !== 0){
        nodePlaceholder.remove();
    }
};

textTratamiento.onblur = function(){
    if(this.innerText.length === 0){
        var p = document.createElement("p"); 
        p.setAttribute("class", "placeholder");
        p.setAttribute("id","placeholder-tratamiento");
        p.textContent = "Introduzca la prescripción...";
        this.append(p);
    }
};

textTratamiento.onkeyup = function(e) {  
   
     if(e.keyCode === 32){                        
        bufferTratamiento = "";
        countCharTratamiento = 0;
    }else if(e.keyCode === 8){                        
        bufferTratamiento = bufferTratamiento.slice(0, -1);
        countCharTratamiento--;
        $('ul[data-box-type="diagnostico"]').remove();
    }else{
        var entrada = String.fromCharCode(e.keyCode);
         if (/[a-zA-Z]/.test(entrada)){
            bufferTratamiento+=e.key; 
            countCharTratamiento++;

             socket.emit('new-tratamiento', bufferTratamiento);

            socket.on('result-tratamiento', function(dato){
                console.log(dato);
            });
            
            $('ul[data-box-type="tratamiento"]').remove();
            

            $.each(json.people.person, function(i, v) {

                if (v.name.search(new RegExp('^'+bufferTratamiento, 'i'), '') !== -1) {

                    bufferItemTratamiento.push(v.name);
                    return;
                }
            });                

            if(bufferItemTratamiento.length !== 0){
                var contenedor = document.createElement("ul");
                contenedor.className  = "box-tags";
                contenedor.setAttribute('data-box-type', 'tratamiento');

                bufferItemTratamiento.forEach(function(element, index) {
                var tag = document.createElement("li");
                    tag.innerHTML = '<span class="badge tags" id="'+index+'"> '+element.toString()+'</span>';                    
                    tag.onclick = function(){
                        $('ul[data-box-type="tratamiento"]').remove();
                    var tratamiento = document.getElementById('tratamiento');
                    var textTratamiento = $('#tratamiento').html();
                    var textActualizado;
                        var me = this;
                        
                        //borrar caracteres por los que se predice
                        if(textTratamiento.length>=1){
                            textActualizado = tratamiento.innerHTML.slice(0,-(countCharTratamiento+('<div contenteditable="true"></div>'.length)));

                        }else{
                            textActualizado = tratamiento.innerHTML.slice(0,-(countCharTratamiento));
                        }
                        $('#tratamiento').html(textActualizado);
                        //texto con el termino seleccionado
                        var tamano=me.childNodes[0].textContent.length;
                        tamano*=7; //el valor multiplicativo debe cambiarse dependiendo del tamaño de la fuente

                        var input = document.createElement("input"); 
                        input.setAttribute("style", 'color:red; border: none; width:'+tamano+'px;"');
                        input.setAttribute("value", me.childNodes[0].textContent);

                        $('#tratamiento').append(input);

                        var divSiguiente = document.createElement("div");  
                        divSiguiente.setAttribute("contenteditable", "true");
                        $('#tratamiento').append(divSiguiente);
                        
                        /*Gana el cursor luego de seleccionar el tag */
                        var divTratamiento = document.getElementById('tratamiento');
                        let s = window.getSelection();
                        let r = document.createRange();
                        r.setStart(divTratamiento, divTratamiento.childElementCount);
                        r.setEnd(divTratamiento, divTratamiento.childElementCount);
                        s.removeAllRanges();
                        s.addRange(r);
                        
                        countCharTratamiento = 0;                                
                        bufferTratamiento = "";


                    };            
                    contenedor.appendChild(tag);
                });
                
                var tratamiento = document.getElementById('tratamiento');
                tratamiento.appendChild(contenedor);            
            }

            bufferItemTratamiento = []; //reiniciar lista de items
        }
    }
};

