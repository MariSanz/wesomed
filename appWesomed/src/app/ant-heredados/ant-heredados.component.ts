import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ant-heredados',
  templateUrl: './ant-heredados.component.html',
  styleUrls: ['./ant-heredados.component.css']
})
export class AntHeredadosComponent implements OnInit {
  @Input() selectedValuesAntecedentes: string[] = [];

  constructor() { }

  ngOnInit() {
  }

}
