import { Component, OnInit, Injector, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/throttleTime';
import {HistoriaClinicaAltaComponent} from '../historia-clinica-alta/historia-clinica-alta.component'
import { TipoEvaluacionEnum, Termino } from '../_models/index';

@Component({
  selector: 'app-historia-clinica-alta-anamnesis',
  templateUrl: './historia-clinica-alta-anamnesis.component.html',
  styleUrls: ['./historia-clinica-alta-anamnesis.component.css']
})
export class HistoriaClinicaAltaAnamnesisComponent implements OnInit {

  parent : HistoriaClinicaAltaComponent;

  //campos des texto
  textAnamnesis: any;

  //eventos observables de keyup
  keyupSubAnamnesis: Subscription;

  //propiedades acumuladores y array resultados de busquedad terms en Anamnesis
  bufferItemAnamnesis: Array<any> = new Array();
  arrayResultadosAnamnesis: Array<any> = new Array();
  arrayResultadosMostrarAnamnesis: Array<any> = new Array();
  indexFromAnamnesis: number = 0;
  bufferAnamnesis = "";

  terminosEnAnamnesis: Array<Termino> = new Array();

  caracterAnteriorAnamnesis: number;

  mostrarSugerenciasAnamnesis: Boolean = false;

  constructor(private ngzone: NgZone, private inj:Injector) {
    this.parent = this.inj.get(HistoriaClinicaAltaComponent);
   }

  ngOnInit() {
  }

  ngAfterViewInit() {

    this.textAnamnesis = document.getElementById('textAnamnesis');
    this.ngzone.runOutsideAngular(() => {
      this.keyupSubAnamnesis = Observable.fromEvent(document.getElementById('textAnamnesis'), 'keyup')
        .debounceTime(300).subscribe((keyboardEvent => {
          this.keyupAnamnesis(keyboardEvent);
        }));
    });
  }

  /**
 * 
 */
  moveToPreviousAnamnesis() {
    this.indexFromAnamnesis -= 4;
    if (this.indexFromAnamnesis < 0) {
      this.indexFromAnamnesis = this.arrayResultadosAnamnesis.length - 4;
    }
    this.arrayResultadosMostrarAnamnesis = this.arrayResultadosAnamnesis.slice(this.indexFromAnamnesis, this.indexFromAnamnesis + 4);
  }

  /**
   * 
   */
  moveToNextAnamnesis() {
    this.indexFromAnamnesis += 4;
    if (this.indexFromAnamnesis >= this.arrayResultadosAnamnesis.length) {
      this.indexFromAnamnesis = 0;
    }
    this.arrayResultadosMostrarAnamnesis = this.arrayResultadosAnamnesis.slice(this.indexFromAnamnesis, this.indexFromAnamnesis + 4);
  }

    /**
   * 
   * @param event 
   */
  protected keyupAnamnesis(event) {
    if (this.parent.sugerencias) {
        if (event.keyCode === 8) { //borrar un caracter de derecha a izquierda 
          this.bufferAnamnesis = this.bufferAnamnesis.slice(0, -1); //borra un caracter
          this.caracterAnteriorAnamnesis = event.keyCode;

          //
          this.terminosEnAnamnesis = this.parent.actualizarTerminos(this.terminosEnAnamnesis , this.textAnamnesis );

        } else {

          let arrayPalabras: Array<string> = this.textAnamnesis.value.split(' ');
          let palabras = this.parent.buscarPalabras(arrayPalabras);
          //si se obtienen correctamente las palabras a buscar entonces se asigna, si no se mantiene el valor bufferAnamnesis
          this.bufferAnamnesis = palabras != null ? palabras : this.bufferAnamnesis;

          if (this.bufferAnamnesis != undefined && this.bufferAnamnesis.length-1 != NaN) {
            this.caracterAnteriorAnamnesis = this.bufferAnamnesis.toUpperCase().charCodeAt(this.bufferAnamnesis.length - 1);
          } else {
            this.caracterAnteriorAnamnesis = event.keyCode;
          }
        } //fin no es espacio 8 la tecla

        //lanzar evaluacion de bufferAnamnesis
        this.evaluar();
    }

  }

    /**
   * Evento de seleccionar un termino en la lista resultado, se borra la palabra correspondiente 
   * y se reemplaza con el termino
   * @param event 
   */
  protected selectItemAnamnesis(result:Termino) {
  
    let termSelect = result.term;
    if (termSelect != undefined) {
      let arrayPalabras: Array<string> = this.textAnamnesis.value.split(' ');
      let replaceText : string;

      let palabras = this.parent.buscarPalabras(arrayPalabras);
     
      replaceText = palabras != null ? palabras :  replaceText = arrayPalabras[arrayPalabras.length - 1];
      
      let textModificado = this.textAnamnesis.value.replace(replaceText, termSelect);
      this.textAnamnesis.value = textModificado;
      this.textAnamnesis.focus();

      //add termino al array
      this.terminosEnAnamnesis.push(result);
    }
    this.mostrarSugerenciasAnamnesis = false;
    this.arrayResultadosMostrarAnamnesis = new Array();
  }

  /**
   * 
   */
  evaluar() {

  let data = {
    term: this.bufferAnamnesis
  };

  var me = this;
  this.parent.socket.emit('new', data);

  this.parent.socket.on('result', function (busqueda) {

    let resultado = busqueda.dato;    
    me.arrayResultadosAnamnesis = new Array();

    var json = JSON.parse(JSON.stringify(resultado));

    json.forEach(element => {

      if (me.parent.areasSelect.includes(me.parent.areasItem[0].value) ||
        me.parent.areasSelect.map(select => select.name.toLowerCase()).filter(string => string === element.semanticTag).length != 0 ||
        me.parent.areasSelect.includes(me.parent.areasItem[5].value)) {
    
        me.bufferItemAnamnesis.push(element.term);
        me.arrayResultadosAnamnesis.push(element);
      }
      return;     
    });

  
    if (me.bufferItemAnamnesis.length !== 0) {
      me.parent.activeResults(TipoEvaluacionEnum.ANAMNESIS);
      me.arrayResultadosMostrarAnamnesis = me.arrayResultadosAnamnesis.slice(0, 4);
    }

    //limpiar caja resultados seleccionables
    if (resultado.length == 0) {
      me.parent.cleanResults();
    }
  
    me.bufferItemAnamnesis = []; //reiniciar lista de items

  }); //fin socket on

}

}
