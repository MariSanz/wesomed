import { Component, OnInit, NgZone, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/throttleTime';
import { HistoriaClinicaAltaComponent } from '../historia-clinica-alta/historia-clinica-alta.component'
import { TipoEvaluacionEnum, Termino } from '../_models';

@Component({
  selector: 'app-historia-clinica-alta-tratamiento',
  templateUrl: './historia-clinica-alta-tratamiento.component.html',
  styleUrls: ['./historia-clinica-alta-tratamiento.component.css']
})
export class HistoriaClinicaAltaTratamientoComponent implements OnInit {

  parent: HistoriaClinicaAltaComponent;

  textTratamiento: any;
  keyupSubTratamiento: Subscription;

  bufferItemTratamiento: Array<any> = new Array();
  arrayResultadosTratamiento: Array<any> = new Array();
  arrayResultadosMostrarTratamiento: Array<any> = new Array();
  indexFromTratamiento: number = 0;
  bufferTratamiento = "";

  terminosEnTratamiento: Array<Termino> = new Array();

  mostrarSugerenciasTratamiento: Boolean = false;

  caracterAnteriorTratamiento: number;

  constructor(private ngzone: NgZone,
    private inj: Injector) {
    this.parent = this.inj.get(HistoriaClinicaAltaComponent);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {    

    this.textTratamiento= document.getElementById('textTratamiento');
    this.ngzone.runOutsideAngular(() => {
      this.keyupSubTratamiento = Observable.fromEvent(document.getElementById('textTratamiento'), 'keyup')
        .debounceTime(300).subscribe((keyboardEvent => {  
          this.keyupTratamiento(keyboardEvent);         
        }));
    });
 }

 
 keyupTratamiento(event){
  if (this.parent.sugerencias) {
    if (event.keyCode === 8) { //borrar un caracter de derecha a izquierda 
      
      this.bufferTratamiento = this.bufferTratamiento.slice(0, -1); //borra un caracter
      this.caracterAnteriorTratamiento = event.keyCode;
      
      this.terminosEnTratamiento = this.parent.actualizarTerminos(this.terminosEnTratamiento , this.textTratamiento );
      
    } else {

      let arrayPalabras: Array<string> = this.textTratamiento.value.split(' ');
      let palabras = this.parent.buscarPalabras(arrayPalabras);
      //si se obtienen correctamente las palabras a buscar entonces se asigna, si no se mantiene el valor bufferTratamiento
      this.bufferTratamiento = palabras != null ? palabras : this.bufferTratamiento;

      if (this.bufferTratamiento != undefined && this.bufferTratamiento.length-1 != NaN) {
        this.caracterAnteriorTratamiento = this.bufferTratamiento.toUpperCase().charCodeAt(this.bufferTratamiento.length - 1);
      } else {
        this.caracterAnteriorTratamiento = event.keyCode;
      }
    } //fin no es espacio 8 la tecla

    //lanzar evaluacion de bufferDiagnostico
    this.evaluar();
  }
}


  /**
   * 
   */
  moveToPreviousTratamiento() {
    this.indexFromTratamiento -= 4;
    if (this.indexFromTratamiento < 0) {
      this.indexFromTratamiento = this.arrayResultadosTratamiento.length - 4;
    }
    this.arrayResultadosMostrarTratamiento = this.arrayResultadosTratamiento.slice(this.indexFromTratamiento, this.indexFromTratamiento + 4);
  }

  /**
   * 
   */
  moveToNextTratamiento() {
    this.indexFromTratamiento += 4;
    if (this.indexFromTratamiento >= this.arrayResultadosTratamiento.length) {
      this.indexFromTratamiento = 0;
    }
    this.arrayResultadosMostrarTratamiento = this.arrayResultadosTratamiento.slice(this.indexFromTratamiento, this.indexFromTratamiento + 4);
  }

     /**
   * Evento de seleccionar un termino en la lista resultado, se borra la palabra correspondiente 
   * y se reemplaza con el termino
   * @param result <Termino>
   */ 
  protected selectItemTratamiento(result:Termino) {
  
    let termSelect = result.term;
    if (termSelect != undefined) {
      let arrayPalabras: Array<string> = this.textTratamiento.value.split(' ');
      let replaceText : string;

      let palabras = this.parent.buscarPalabras(arrayPalabras);
      replaceText = palabras != null ? palabras :  replaceText = arrayPalabras[arrayPalabras.length - 1];
         
      let textModificado = this.textTratamiento.value.replace(replaceText, termSelect);
      this.textTratamiento.value = textModificado;
      this.textTratamiento.focus();

      //add termino al array
      this.terminosEnTratamiento.push(result);
    }
    this.mostrarSugerenciasTratamiento = false;
    this.arrayResultadosMostrarTratamiento = new Array();
  }

  /**
   * 
   */
  evaluar() {

    let data = {
      term: this.bufferTratamiento
    };
  
    var me = this;
    this.parent.socket.emit('new', data);
  
    this.parent.socket.on('result', function (busqueda) {
  
      let resultado = busqueda.dato;    
      me.arrayResultadosTratamiento = new Array();
  
      var json = JSON.parse(JSON.stringify(resultado));
  
      json.forEach(element => {
  
        if (me.parent.areasSelect.includes(me.parent.areasItem[0].value) ||
          me.parent.areasSelect.map(select => select.name.toLowerCase()).filter(string => string === element.semanticTag).length != 0 ||
          me.parent.areasSelect.includes(me.parent.areasItem[5].value)) {
      
          me.bufferItemTratamiento.push(element.term);
          me.arrayResultadosTratamiento.push(element);
        }
        return;     
      });
  
    
      if (me.bufferItemTratamiento.length !== 0) {
        me.parent.activeResults(TipoEvaluacionEnum.TREATMENT);
        me.arrayResultadosMostrarTratamiento= me.arrayResultadosTratamiento.slice(0, 4);
      }
  
      //limpiar caja resultados seleccionables
      if (resultado.length == 0) {
        me.parent.cleanResults();
      }
    
      me.bufferItemTratamiento = []; //reiniciar lista de items
  
    }); //fin socket on
  
  }
}
