import { Component } from '@angular/core';

import { User, Logger, LoggerMessage } from './_models/index';
import { LoggerService } from './_services/logger.service';
import { LogLevelEnum } from './_models/_enum/logLevelEnum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentUser: User =  null;
  title = 'WESOMED';

  handlerError(loggerService : LoggerService, log: Logger, exp, component:string) {
    log.message =  new LoggerMessage( exp.error.message, LogLevelEnum.ERROR, component);
    loggerService.create(log);
  }

  loadLog(log: Logger, component:string){
    if(JSON.parse(localStorage.getItem('currentUser')) != undefined) {
      log.user = JSON.parse(localStorage.getItem('currentUser')).username;
    } else {
      log.user = 'ANONIMO';
    }
    
    log.message = new LoggerMessage( 'loadLog', LogLevelEnum.INFO, component);
  }

  createLog(loggerService : LoggerService, log:Logger,  text:String, source: String) {
    
    log.message = new LoggerMessage( text, LogLevelEnum.TRACE, source);
    loggerService.create(log);
  }
}
