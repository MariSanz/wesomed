﻿import { Component, OnInit, NgZone, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User, Patient, Logger } from '../_models/index';
import { UtilList } from '../_helpers/index';
import { UserService, PatientService , AlertService} from '../_services/index';
import { LoggerService } from '../_services/logger.service';
import { LogLevelEnum } from '../_models/_enum/logLevelEnum';
import { AppComponent } from '../app.component';

@Component({
    templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {
    currentUser: User;
    log: Logger  = new Logger();
    model: any = {};
    users: User[] = [];
    pacientes: Patient[] = [];
    cols: any[];
    returnUrl: string;
    historiaUrl : String;

    parent : AppComponent;


    constructor(
        private userService: UserService,
        private patientService: PatientService, 
        private alertService : AlertService,
        private loggerService : LoggerService,
        private route: ActivatedRoute,
        private router: Router,    
        private utilList :  UtilList,
        private inj:Injector
        ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.parent = this.inj.get(AppComponent);
        this.parent.loadLog(this.log, 'Home');
    }

    ngOnInit() {
        this.pacientes = null;

        this.historiaUrl = 'paciente';
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.cols = [          
            { field: 'nss', header: 'Nº Seguridad Social' },
            { field: 'name', header: 'Nombre' },
            { field: 'surname', header: 'Apellidos' },
            { field: 'age', header: 'Edad' },
            { field: 'ruta', header: 'Opciones' }
        ];
        this.findPacientes();
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(
            () => { 
                this.loadAllUsers();
             },
            exp =>{ 
                this.parent.handlerError(this.loggerService, this.log, exp, 'Home');
            });
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe(
            users => { 
                this.users = users;
                this.parent.createLog( this.loggerService, this.log, 'loadAllUsers -> '+ users.length, 'Home');
             },
            exp => {
                this.parent.handlerError(this.loggerService, this.log, exp, 'Home');
            });
    }

    findPacientes() {        
        this.composeModel();
       
        if (this.model) {
            this.patientService.getByModelPaciente( this.model ).subscribe(
                data => {
                  
                    this.parent.createLog( this.loggerService, this.log, 'findPacientes', 'Home');
                    this.returnPatientData(data);
                },
                exp => {
                    this.returnError(exp.error.message);
                    this.parent.handlerError(this.loggerService, this.log, exp, 'Home');
                });
                return;
        }
       
    }

    private composeModel()  {       
        this.model.surname = undefined;
        if(this.model.nss == '') {
            this.model.nss = undefined;
        }
        if(this.model.name == '') {
            this.model.name = undefined;
        }
    }

    private returnPatientData(data){       
        this.pacientes = data;
      
        this.pacientes.forEach( (elem, index) =>{           
            elem.ruta = '<a href="/paciente/';
            elem.ruta += elem.nss;
            elem.ruta += '"><i class="fa fa-search" /></a>';          
        });
     
        this.router.navigate([this.returnUrl]);
    }

    private returnError(error){
        this.alertService.error(error);
    }

    showHistoriaClinica(patient: Patient){
        this.router.navigate([this.historiaUrl]);
    }
}