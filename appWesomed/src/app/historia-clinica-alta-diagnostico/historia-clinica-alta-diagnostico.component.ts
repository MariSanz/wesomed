import { Component, OnInit, NgZone, Injector  } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/throttleTime';
import {HistoriaClinicaAltaComponent} from '../historia-clinica-alta/historia-clinica-alta.component'
import { TipoEvaluacionEnum, Termino } from '../_models';


@Component({
  selector: 'app-historia-clinica-alta-diagnostico',
  templateUrl: './historia-clinica-alta-diagnostico.component.html',
  styleUrls: ['./historia-clinica-alta-diagnostico.component.css']
})
export class HistoriaClinicaAltaDiagnosticoComponent implements OnInit {

  parent : HistoriaClinicaAltaComponent;

  textDiagnostico: any;
  keyupSubDiagnostico: Subscription;

  bufferItemDiagnostico: Array<any> = new Array();
  arrayResultadosDiagnostico: Array<any> = new Array();
  arrayResultadosMostrarDiagnostico: Array<any> = new Array();
  indexFromDiagnostico: number = 0;
  bufferDiagnostico = "";

  terminosEnDiagnostico : Array<Termino> = new Array();

  mostrarSugerenciasDiagnostico: Boolean = false;

  caracterAnteriorDiagostico: number;


  constructor(private ngzone: NgZone,
    private inj:Injector
  ) { 
    this.parent = this.inj.get(HistoriaClinicaAltaComponent);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {    

     this.textDiagnostico = document.getElementById('textDiagnostico');
     this.ngzone.runOutsideAngular(() => {
       this.keyupSubDiagnostico = Observable.fromEvent(document.getElementById('textDiagnostico'), 'keyup')
         .debounceTime(300).subscribe((keyboardEvent => {  
           this.keyupDiagnostico(keyboardEvent);         
         }));
     });
  }

  keyupDiagnostico(event){
    if (this.parent.sugerencias) {
      if (event.keyCode === 8) { //borrar un caracter de derecha a izquierda 
       
        this.bufferDiagnostico = this.bufferDiagnostico.slice(0, -1); //borra un caracter
        this.caracterAnteriorDiagostico = event.keyCode;
        
        this.terminosEnDiagnostico = this.parent.actualizarTerminos(this.terminosEnDiagnostico , this.textDiagnostico );
        
      } else {

        let arrayPalabras: Array<string> = this.textDiagnostico.value.split(' ');
        let palabras = this.parent.buscarPalabras(arrayPalabras);
        //si se obtienen correctamente las palabras a buscar entonces se asigna, si no se mantiene el valor bufferDiagnostico
        this.bufferDiagnostico = palabras != null ? palabras : this.bufferDiagnostico;

        if (this.bufferDiagnostico != undefined && this.bufferDiagnostico.length-1 != NaN) {
          this.caracterAnteriorDiagostico = this.bufferDiagnostico.toUpperCase().charCodeAt(this.bufferDiagnostico.length - 1);
        } else {
          this.caracterAnteriorDiagostico = event.keyCode;
        }
      } //fin no es espacio 8 la tecla

      //lanzar evaluacion de bufferDiagnostico
      this.evaluar();
    }
  }

  /**
   * 
   */
  moveToPreviousDiagnostico() {
    this.indexFromDiagnostico -= 4;
    if (this.indexFromDiagnostico < 0) {
      this.indexFromDiagnostico = this.arrayResultadosDiagnostico.length - 4;
    }
    this.arrayResultadosMostrarDiagnostico = this.arrayResultadosDiagnostico.slice(this.indexFromDiagnostico, this.indexFromDiagnostico + 4);
  }

  /**
   * 
   */
  moveToNextAnamnesis() {
    this.indexFromDiagnostico += 4;
    if (this.indexFromDiagnostico >= this.arrayResultadosDiagnostico.length) {
      this.indexFromDiagnostico = 0;
    }
    this.arrayResultadosMostrarDiagnostico = this.arrayResultadosDiagnostico.slice(this.indexFromDiagnostico, this.indexFromDiagnostico + 4);
  }

    /**
   * Evento de seleccionar un termino en la lista resultado, se borra la palabra correspondiente 
   * y se reemplaza con el termino
   * @param result <Termino>
   */ 
  protected selectItemDiagnostico(result:Termino) {
  
    let termSelect = result.term;
    if (termSelect != undefined) {
      let arrayPalabras: Array<string> = this.textDiagnostico.value.split(' ');
      let replaceText : string;

      let palabras = this.parent.buscarPalabras(arrayPalabras);
      replaceText = palabras != null ? palabras :  replaceText = arrayPalabras[arrayPalabras.length - 1];
         
      let textModificado = this.textDiagnostico.value.replace(replaceText, termSelect);
      this.textDiagnostico.value = textModificado;
      this.textDiagnostico.focus();

      //add termino al array
      this.terminosEnDiagnostico.push(result);
    }
    this.mostrarSugerenciasDiagnostico = false;
    this.arrayResultadosMostrarDiagnostico = new Array();
  }

  /**
   * 
   */
  evaluar() {

    let data = {
      term: this.bufferDiagnostico
    };
  
    var me = this;
    this.parent.socket.emit('new', data);
  
    this.parent.socket.on('result', function (busqueda) {
  
      let resultado = busqueda.dato;    
      me.arrayResultadosDiagnostico = new Array();
  
      var json = JSON.parse(JSON.stringify(resultado));
  
      json.forEach(element => {
  
        if (me.parent.areasSelect.includes(me.parent.areasItem[0].value) ||
          me.parent.areasSelect.map(select => select.name.toLowerCase()).filter(string => string === element.semanticTag).length != 0 ||
          me.parent.areasSelect.includes(me.parent.areasItem[5].value)) {
      
          me.bufferItemDiagnostico.push(element.term);
          me.arrayResultadosDiagnostico.push(element);
        }
        return;     
      });
  
    
      if (me.bufferItemDiagnostico.length !== 0) {
        me.parent.activeResults(TipoEvaluacionEnum.DIAGNOSTIC);
        me.arrayResultadosMostrarDiagnostico = me.arrayResultadosDiagnostico.slice(0, 4);
      }
  
      //limpiar caja resultados seleccionables
      if (resultado.length == 0) {
        me.parent.cleanResults();
      }
    
      me.bufferItemDiagnostico = []; //reiniciar lista de items
  
    }); //fin socket on
  
  }
  

}
