import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/throttleTime';
import * as io from "socket.io-client";
import { MenuItem, SelectItem } from "primeng/api";
import { TipoEvaluacionEnum, Area, Termino, HistoriaClinica, DiagnosticReportStatusEnum, Practitioner, User, Observation, ObservationStatusEnum, EncounterClassEnum, Diagnosis, TerminoSnomed, Encounter, Patient, Logger, Reporte } from '../_models/index';

import { HistoriaClinicaAltaDiagnosticoComponent } from '../historia-clinica-alta-diagnostico/historia-clinica-alta-diagnostico.component';
import { HistoriaClinicaAltaAnamnesisComponent } from '../historia-clinica-alta-anamnesis/historia-clinica-alta-anamnesis.component';
import { HistoriaClinicaAltaTratamientoComponent } from '../historia-clinica-alta-tratamiento/historia-clinica-alta-tratamiento.component';
import { PacienteComponent } from '../paciente';

import { HistoriaClinicaService, AlertService, ReporteService } from '../_services';
import { AppComponent } from '../app.component';
import { LoggerService } from '../_services/logger.service';


@Component({
  selector: 'app-historia-clinica-alta',
  templateUrl: './historia-clinica-alta.component.html',
  styleUrls: ['./historia-clinica-alta.component.css']
})
export class HistoriaClinicaAltaComponent implements OnInit {
  socket: any = null;
  items: MenuItem[];

  parentPaciente: PacienteComponent;
  parent : AppComponent;

  @ViewChild(HistoriaClinicaAltaAnamnesisComponent)
  private childAnamnesis: HistoriaClinicaAltaAnamnesisComponent;

  @ViewChild(HistoriaClinicaAltaDiagnosticoComponent)
  private childDiagnostico: HistoriaClinicaAltaDiagnosticoComponent;

  @ViewChild(HistoriaClinicaAltaTratamientoComponent)
  private childTratamiento: HistoriaClinicaAltaTratamientoComponent;


  //campos des texto
  textTratamiento: any;

  //eventos observables de keyup
  keyupSubTratamiento: Subscription;

  sugerencias: Boolean;
  numeroPalabrasBuscar: number = 1;
  areasItem: SelectItem[];
  areasSelect: Area[] = new Array();

  mostrarSugerenciasTreatment: Boolean = true;
  mostrarSugerenciasAnamnesis: Boolean = true;
  mostrarSugerenciasDiagnostico: Boolean = true;

  log: Logger  = new Logger();

  constructor(
    private router: Router, 
    private inj: Injector,
    private loggerService : LoggerService,
    private historiaService: HistoriaClinicaService,
    private reporteService: ReporteService,
     private alertService: AlertService) {
    this.socket = io('http://18.188.197.91:88');
    this.sugerencias = true;

    this.parent = this.inj.get(AppComponent);
    this.parent.loadLog(this.log, 'Historia-Clinica-Alta');
    this.parentPaciente = this.inj.get(PacienteComponent);
  }

  ngOnInit() {

    this.areasItem = [
      { label: 'Todas', value: { id: 1, name: 'Todas', code: 'TD' } },
      { label: 'Hallazgo', value: { id: 2, name: 'Hallazgo', code: 'HZ' } },
      { label: 'Procedimiento', value: { id: 3, name: 'Procedimiento', code: 'PD' } },
      { label: 'Trastorno', value: { id: 4, name: 'Trastorno', code: 'TT' } },
      { label: 'Sustancia', value: { id: 5, name: 'Sustancia', code: 'ST' } },
      { label: 'Otros', value: { id: 6, name: 'Otras', code: 'OT' } }
    ];

    //check por defecto todas las areas
    this.areasSelect.push(this.areasItem[0].value);

    // SOCKET 

    //Connection opened
    this.socket.addEventListener('open', function (event) {
      console.log("Connected to server");
    });
    //Listen to messages
    this.socket.on('messages', function (dato) { });
  }

  ngAfterViewInit() { }

  /**
   * 
   * @param tipoEvaluacion 
   */
  public activeResults(tipoEvaluacion: TipoEvaluacionEnum) {
    switch (tipoEvaluacion) {
      case TipoEvaluacionEnum.ANAMNESIS:
        this.childAnamnesis.mostrarSugerenciasAnamnesis = true;
        this.childDiagnostico.mostrarSugerenciasDiagnostico = false;
        this.childTratamiento.mostrarSugerenciasTratamiento = false;
        break;
      case TipoEvaluacionEnum.DIAGNOSTIC:
        this.childAnamnesis.mostrarSugerenciasAnamnesis = false;
        this.childDiagnostico.mostrarSugerenciasDiagnostico = true;
        this.childTratamiento.mostrarSugerenciasTratamiento = false;
        break;
      case TipoEvaluacionEnum.TREATMENT:
        this.childAnamnesis.mostrarSugerenciasAnamnesis = false;
        this.childDiagnostico.mostrarSugerenciasDiagnostico = false;
        this.childTratamiento.mostrarSugerenciasTratamiento= true;
        break;
      default:
        throw new Error('Error - TipoEvaluacion no soportada ' + tipoEvaluacion);
    }
  }

  /**
   * Se ocultan todas las cajas de resultados de los terminos en las campos de texto
   */
  cleanResults() {
    this.childAnamnesis.mostrarSugerenciasAnamnesis = false;
    this.childDiagnostico.mostrarSugerenciasDiagnostico = false;
    this.mostrarSugerenciasTreatment = false;
  }

  public buscarPalabras(arrayPalabras: string[]) {
    let palabras;
    if (this.numeroPalabrasBuscar == 1) {
      //busca de una en una palabras
      palabras = arrayPalabras[arrayPalabras.length - 1];
     
      if (palabras == undefined) {
        palabras = arrayPalabras[arrayPalabras.length - 2];
      }
    }
    else { //palabras == 2
      //busca de dos en dos palabras
      let antepenultimaPalabra = arrayPalabras[arrayPalabras.length - 2] != undefined ? arrayPalabras[arrayPalabras.length - 2] : '';
      let ultimaPalabra = arrayPalabras[arrayPalabras.length - 1] != undefined ? arrayPalabras[arrayPalabras.length - 1] : '';
      palabras = antepenultimaPalabra.concat(' ').concat(ultimaPalabra);
    } //fin if palabraBuscar
    if (/[a-zA-Z]/.test(palabras)) {
      return palabras;
    }
    return null;
  }

  /**
   * 
   */
  public actualizarTerminos(terminosArray: Array<Termino>, textBuscar: any) {
    return terminosArray.filter(item => {
      (<String>textBuscar.value).includes(item.term);
    });
  }


  guardarHistoria(event) {
   
    let formPatientThis : Patient = this.parentPaciente.paciente;
    let formPatient : Patient = new Patient(formPatientThis.nss, formPatientThis.name, formPatientThis.surname, formPatientThis.name, formPatientThis.gender, 
      formPatientThis.age, formPatientThis.birthdate, formPatientThis.telecom );
    
    let user: User = JSON.parse(localStorage.getItem('currentUser'));
    let formPractitioner : Practitioner = new Practitioner(user.username, undefined, user.lastName, user.firstName, undefined, undefined, undefined, undefined);

    let { formEncounterAnamnesis, formContextAnamnesis, fromResultObservationAnamnesis } = this.formObservationAnamnesis();

    let { fromResultObservationDiagnostico, formEncounterDiagnostico, formContextDiagnostico } = this.formObservationDiagnostico();

    let fromResultObservationTratamiento = this.formObservationTratamiento();

    var formHistoria = new HistoriaClinica(new Date(), DiagnosticReportStatusEnum.REGISTERED, formPatient, formPractitioner);
   
    formHistoria.addResult(fromResultObservationAnamnesis);
    formHistoria.addResult(fromResultObservationDiagnostico);
    formHistoria.addResult(fromResultObservationTratamiento);
    
    this.historiaService.create(formHistoria)
      .subscribe(
        data => {
          var historiaClinidaGuardada = JSON.stringify(data);
          this.parent.createLog( this.loggerService, this.log, 'guardarHistoria -> '+ historiaClinidaGuardada, 'Historia-Clinica-Alta');
        
          this.guardarReporteHistoriaClinica(formPatient);
          this.parentPaciente.loadHistoriasClinicas();
          this.parentPaciente.loadReporteHistoriaClinicaByPatient(formPatient.identifier);
          this.alertService.success('Se ha guardado la Historia Clínica successful', true);

        },
        exp => {   
          this.parent.handlerError(this.loggerService, this.log, exp, 'Historia-Clinica-Alta');
          this.alertService.error(exp);
          this.parentPaciente.loadHistoriasClinicas();
        },
        () => {
          let rutaThis = '/paciente/'+this.parentPaciente.paciente.nss;
          this.router.navigate([rutaThis]);
          this.parentPaciente.activarTabHistoriaClinica();
        });
}

private guardarReporteHistoriaClinica(formPatient: Patient) {
  var tratamiento = this.childTratamiento.textTratamiento.value;
    if(tratamiento == null) {
      tratamiento = '';
    }
    const formReporte = new Reporte(
      formPatient,
      this.childAnamnesis.textAnamnesis.value, 
      this.childDiagnostico.textDiagnostico.value,
      tratamiento,
      ''
      );
    this.reporteService.create(formReporte);
  }

  /**
   * 
   */
  private formObservationTratamiento() {
    const formEncounter = new Encounter(EncounterClassEnum.AMB);
   
    this.childTratamiento.terminosEnTratamiento.forEach(element => {
      const snomedTermThis = new TerminoSnomed('http://purl.bioontology.org/ontology/SNOMEDCT/' + element.conceptId, element.descriptionId, element.term);
      formEncounter.addDiagnosisConstructor(element.conceptId, 'http://snomed.info/sct', element.semanticTag, snomedTermThis);
    });
    let formContext = {
      context: {
        encounter: formEncounter
      }
    };
    const fromResultObservationTratamiento = new Observation(ObservationStatusEnum.FINAL, formEncounter);
    return fromResultObservationTratamiento;
  }

  /**
   * 
   */
  private formObservationDiagnostico() {
    const formEncounterDiagnostico = new Encounter(EncounterClassEnum.AMB);
    this.childDiagnostico.terminosEnDiagnostico.forEach(element => {
      const snomedTermThis = new TerminoSnomed('http://purl.bioontology.org/ontology/SNOMEDCT/' + element.conceptId, element.descriptionId, element.term);
      formEncounterDiagnostico.addDiagnosisConstructor(element.conceptId, 'http://snomed.info/sct', element.semanticTag, snomedTermThis);
    });
    const formContextDiagnostico = {
      context: {
        encounter: formEncounterDiagnostico
      }
    };
    const fromResultObservationDiagnostico = new Observation(ObservationStatusEnum.FINAL, formEncounterDiagnostico);
    return { fromResultObservationDiagnostico, formEncounterDiagnostico, formContextDiagnostico };
  }

  /**
   * 
   */
  private formObservationAnamnesis() {
    const formEncounterAnamnesis = new Encounter(EncounterClassEnum.AMB);
    this.childAnamnesis.terminosEnAnamnesis.forEach(element => {
      const snomedTermThis = new TerminoSnomed('http://purl.bioontology.org/ontology/SNOMEDCT/' + element.conceptId, element.descriptionId, element.term);
      formEncounterAnamnesis.addDiagnosisConstructor(element.conceptId, 'http://snomed.info/sct', element.semanticTag, snomedTermThis);
    });
    const formContextAnamnesis = {
      context: {
        encounter: formEncounterAnamnesis
      }
    };
    const fromResultObservationAnamnesis = new Observation(ObservationStatusEnum.FINAL, formEncounterAnamnesis);
    return { formEncounterAnamnesis, formContextAnamnesis, fromResultObservationAnamnesis };
  }
}
