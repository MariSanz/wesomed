export * from './historia-clinica-alta.component';
export * from '../historia-clinica-alta-anamnesis/historia-clinica-alta-anamnesis.component';
export * from '../historia-clinica-alta-diagnostico/historia-clinica-alta-diagnostico.component';
export * from '../historia-clinica-alta-tratamiento/historia-clinica-alta-tratamiento.component';
