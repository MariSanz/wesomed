import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ant-no-patologicos',
  templateUrl: './ant-no-patologicos.component.html'
})
export class AntNoPatologicosComponent implements OnInit {
  @Input() selectedValuesAntecedentes: string[] = [];

  constructor() { }

  ngOnInit() {
  }

}
