import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HistoriaClinica } from '../_models/index';
import { UtilList } from '../_helpers/index';
import { environment } from '../../environments/environment';

const urlHistoriaClinica : string = environment.url_services + environment.services.historia_clinica;
@Injectable()
export class HistoriaClinicaService {
    constructor(private http: HttpClient, private utilList : UtilList) { }

    getAll() {
        return this.http.get<HistoriaClinica[]>(urlHistoriaClinica);
    }

    getById(id: number) {
        return this.http.get(urlHistoriaClinica + id);
    }

    create(historia: HistoriaClinica) {   

        return this.http.put(urlHistoriaClinica, historia);
    }

    getByModelHistoriaClinica(historia: HistoriaClinica) {
        let params : Array<string> = [];
       
      
        params = this.utilList.addParamIfExist(historia.date, params);
        params = this.utilList.addParamIfExist(historia.patient.nss, params);
       
        return this.http.get(urlHistoriaClinica + params.join('/'));
    }

    delete(id: number) {
        return this.http.delete(urlHistoriaClinica + id);
    }
}