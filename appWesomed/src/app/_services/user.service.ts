import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/index';
import { environment } from '../../environments/environment';

var urlUser : string = environment.url_services_user + environment.services.user;

@Injectable()
export class UserService {
    
    constructor(private http: HttpClient){}

    getAll() {
        return this.http.get<User[]>(urlUser);
    }

    getById(id: number) {
        return this.http.get<User>(urlUser + id);
    }

    create(user: User) {
        urlUser += 'register';
        return this.http.post(urlUser, user);
    }

    update(user: User) {
        return this.http.put(urlUser + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(urlUser+ id);
    }
}