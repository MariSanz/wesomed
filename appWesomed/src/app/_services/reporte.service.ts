import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Reporte } from '../_models/index';

import { environment } from '../../environments/environment';

const urlReporte : string = environment.url_services + environment.services.reporte;
@Injectable()
export class ReporteService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Reporte[]>(urlReporte);
    }

    getByPatient(idPatient : String) {
        return this.http.get<Reporte[]>(urlReporte + idPatient);
    }

    create(reporte: Reporte) {   
        return this.http.put(urlReporte, reporte).subscribe(
            data => {
                
            },
            error => {
               
            }
        );
    }

    delete(id: number) {
        return this.http.delete(urlReporte + id);
    }
}