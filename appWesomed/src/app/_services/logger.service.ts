import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

import { Logger } from '../_models/index';
import { environment } from "../../environments/environment";

const urlLog : string = environment.url_services + environment.services.log;

@Injectable()
export class LoggerService {

    constructor(private http: HttpClient){}

    getAll() {
        return this.http.get<Logger[]>(urlLog);
    }

    create(log: Logger) {
        return this.http.post(urlLog, log)
        .subscribe(
            error => {
                //console.log("LOGER SERVICE error -> "+ JSON.stringify(error));
            }
        );
    }

    update(log: Logger){
        return this.http.put(urlLog, log)
        .subscribe(
            error => {
                console.log("LOGER SERVICE UPDATE error -> "+error);
            }
        );
    }
}