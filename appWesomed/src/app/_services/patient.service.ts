import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Patient } from '../_models/index';
import { UtilList } from '../_helpers/index';
import { environment } from '../../environments/environment';

const urlPatients : string = environment.url_services + environment.services.patient;

@Injectable()
export class PatientService {
    constructor(private http: HttpClient, private utilList : UtilList) { }

    getAll() {
        return this.http.get<Patient[]>(urlPatients);
    }

    getById(id: number) {
        return this.http.get(urlPatients + id);
    }

    getByModelPaciente(patient: Patient) {
        let params : Array<string> = [];
    
        params = this.utilList.addParamIfExist(patient.nss, params);
        params = this.utilList.addParamIfExist(patient.name, params);
        params = this.utilList.addParamIfExist(patient.surname, params);
      
        return this.http.post(urlPatients, patient);
    }

    delete(id: number) {
        return this.http.delete(urlPatients + id);
    }
}