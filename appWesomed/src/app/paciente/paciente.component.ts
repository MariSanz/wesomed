import { Component, OnInit, Injector } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PatientService, AlertService, HistoriaClinicaService, ReporteService } from '../_services/index';
import { Patient, HistoriaClinica, Logger, Reporte } from '../_models/index';
import { HistoriaClinicaTable } from '../_models/dto/historiaClinicaTable';
import { AppComponent } from '../app.component';
import { LoggerService } from '../_services/logger.service';



@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html'
})
export class PacienteComponent implements OnInit {
  paciente: Patient;
  historial: HistoriaClinica[];
  log: Logger = new Logger();
  showHistoriaClinicaAlta: Boolean = false;

  displayHistoria: boolean = false;
  jsonHistoriaClinica: string;

  selectedValuesAntecedentes: string[] = [];
  historiasClinicas: Array<HistoriaClinicaTable> = new Array();
  historiasClinicasResult: Array<JSON> =new Array();
  reportes: Array<Reporte> =new Array();


  historiasDetalle:  Array<JSON> =new Array();
  colsHistoriasDetalle: any[];
  colsReportesDetalle: any[];

  urlImage: String;

  indexTab: number = 0;

  tabDatosSelect: boolean = true;
  tabAntecedentesSelect: boolean = false;
  tabHistoriaSelect: boolean = false;

  parent : AppComponent;

  constructor(
    private patientService: PatientService,
    private reporteService: ReporteService,
    private alertService: AlertService, 
    private historiaClinicaService: HistoriaClinicaService,
    private loggerService : LoggerService,
    private route: ActivatedRoute,
    private inj:Injector
    ) {
    
    this.parent = this.inj.get(AppComponent);
    this.parent.loadLog(this.log, 'Paciente');

    this.urlImage = 'perfil-azul.png';

    this.paciente = new Patient(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
    this.paciente.nss = this.route.snapshot.params.nss;

    this.loadPatient();
    this.loadHistoriasClinicas();
    this.loadReporteHistoriaClinicaByPatient(this.paciente.identifier);

    this.activarTabDatosGenerales();

    this.colsHistoriasDetalle = [
      { field: 'date', header: 'Fecha' },
      { field: 'practionerFullName', header: 'Médico' }
  ];

  this.colsReportesDetalle = [
    { field: 'fecha', header: 'Fecha' }
];

  }

  ngOnInit() {

  }

  private loadPatient() {
    
    this.patientService.getByModelPaciente(this.paciente).subscribe(
      pacienteResult => {
        
        this.parent.createLog( this.loggerService, this.log, 'loadPatient -> '+ JSON.stringify(pacienteResult), 'Paciente');
        this.returnPatientData(pacienteResult);
      },
      exp => {
       
        this.parent.handlerError(this.loggerService, this.log, exp, 'Paciente');
        this.alertService.error(exp.error.message);
      });
  }

  loadHistoriasClinicas() {
    this.paciente.identifier = this.paciente.nss;
    let historiaFilter = new HistoriaClinica(null, null, this.paciente, null);
    this.historiaClinicaService.getByModelHistoriaClinica(historiaFilter).subscribe(
      historiaResult => {
        this.parent.createLog( this.loggerService, this.log, 'loadHistoriasClinicas -> '+ JSON.stringify(historiaResult), 'Paciente');
        this.returnHistoriasData(historiaResult);
      },
      exp => {
        this.parent.handlerError(this.loggerService, this.log, exp, 'Paciente');
        this.alertService.error(exp.error.message);
      }
    );
  }

  private returnPatientData(data) {
    this.paciente = data[0];
    this.paciente.urlImage = this.urlImage;
  }

  private returnHistoriasData(data) {
    if (data != undefined && data != null && data.length > 0) {
      this.historiasClinicas = new Array();
      this.historiasClinicas = new  Array();
      data.forEach(element => {
        let nombre : Array<any> = new Array( element.practitioner.firstName, element.practitioner.surname );
        let historiaFila = new HistoriaClinicaTable(element.date, element.practitioner.identifier, nombre.join(' '),
          element._id, element.result, element.status );
        this.historiasClinicasResult.push(element);
        this.historiasClinicas.push(historiaFila);
      });
    }
  }
  
  loadReporteHistoriaClinicaByPatient(idPatient: String) {    
    this.reporteService.getByPatient(idPatient).subscribe(

      reporteResult => {
        reporteResult.forEach(element => {
          this.reportes.push(element);
        });
        
      });
  }

  altaHistoriaClinica(evento) {
    this.showHistoriaClinicaAlta = true;
  }

  openNext() {
    this.indexTab = (this.indexTab === 2) ? 0 : this.indexTab + 1;
  }

  openPrev() {
    this.indexTab = (this.indexTab === 0) ? 2 : this.indexTab - 1;
  }

  activarTabHistoriaClinica() {
    this.tabHistoriaSelect = true;
    this.tabAntecedentesSelect = false;
    this.showHistoriaClinicaAlta = false;
    this.tabDatosSelect = false;
  }

  activarTabDatosGenerales() {
    this.tabHistoriaSelect = false;
    this.tabAntecedentesSelect = false;
    this.tabDatosSelect = true;
  }

  verDetalleHistoria(event, id){
    console.log(id);
   
    this.historiasDetalle = new Array();
    this.historiasDetalle.push( this.historiasClinicasResult.find( historia =>  historia['_id'] == id) );
  
    this.displayHistoria = true;
  }


}
