import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService } from '../_services/index';
import { AppComponent } from '../app.component';
import { Logger, LoggerMessage } from '../_models';
import { LoggerService } from '../_services/logger.service';
import { LogLevelEnum } from '../_models/_enum/logLevelEnum';

@Component({
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    log: Logger = new Logger();

    parent : AppComponent;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private loggerService : LoggerService,
        private inj:Injector
        ) { 
            this.parent = this.inj.get(AppComponent);
            this.parent.loadLog(this.log, 'Login');
        }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        this.loggerService.create(this.log);
    

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.log.message = new LoggerMessage('login -> success - ' + this.model.username, LogLevelEnum.TRACE, 'Login');
                
                    this.loggerService.create(this.log);

                    this.router.navigate([this.returnUrl]);
                },
                exp => {
                    this.alertService.error(exp.error.message);
                    this.parent.handlerError(this.loggerService, this.log, exp, 'Login');
                    this.loading = false;
                });
    }
}
