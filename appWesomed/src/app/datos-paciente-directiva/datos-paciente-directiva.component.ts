import { Component, OnInit , Input} from '@angular/core';
import { Patient } from '../_models/index';

@Component({
  selector: 'app-datos-paciente-directiva',
  templateUrl: './datos-paciente-directiva.component.html'  
})
export class DatosPacienteDirectivaComponent implements OnInit {
  @Input()  paciente: Patient;

  constructor() { }

  ngOnInit() {
  }

}
