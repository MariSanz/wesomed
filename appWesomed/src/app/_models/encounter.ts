import { Diagnosis, TerminoSnomed, EncounterClassEnum } from "./index";
import { Coding } from "./coding";

export class Encounter {
    class: String;
    diagnosis: Array<Diagnosis>;

    constructor(_class: EncounterClassEnum) {
        this.class = _class;
        this.diagnosis = new Array();
    }

    addDiagnosisConstructor(code: String,   system: String,  display: String,  snomedterm: TerminoSnomed) {
        let diagnosisParam = new Diagnosis();
        let coding = new Coding(code, system, display);
      
        diagnosisParam.createCondition(coding, snomedterm);
   
        this.diagnosis.push(diagnosisParam);
    }

    addDiagnosis(diagnosis : Diagnosis) {
        this.diagnosis.push(diagnosis);
    }
}