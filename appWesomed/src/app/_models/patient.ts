import { Person } from "./person";

export class Patient extends Person{
    ruta: string;
    urlImage: String;   
    nss: string;
    follow: boolean;    
}