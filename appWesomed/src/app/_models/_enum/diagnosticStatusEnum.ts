export enum DiagnosticReportStatusEnum {       
    REGISTERED = 'registered',
    FINAL = 'final',
    PARTIAL = 'partial',
    CANCELLED = 'cancelled',
    ENTERED_IN_ERROR = 'entered-in-error',
    UNKNOWN    = 'unknown'
  }  ;