export enum EncounterClassEnum {       
    AMB = 'ambulatory',
    EMER = 'emergency',
    HH = 'home health',
    PRENC = 'pre-admission',
    IMP = 'inpatient encounter',
    VR = 'virtual',
    SS = 'short stay',
    UNKNOWN    = 'unknown'
  }  ;