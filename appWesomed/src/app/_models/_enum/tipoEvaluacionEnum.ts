export enum TipoEvaluacionEnum {
    ANAMNESIS = 0,
    DIAGNOSTIC = 1,
    TREATMENT = 2
  }  ;