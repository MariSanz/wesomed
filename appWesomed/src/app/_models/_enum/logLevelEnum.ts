export enum LogLevelEnum { 
    DEBUG = 'debug',
    INFO = 'info',
    ERROR = 'error',
    TRACE = 'trace',
    WARNING = 'warning'
}