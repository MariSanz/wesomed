export class Coding {
    code: String;
    system: String;
    display: String;

    constructor(  code: String,
        system: String,
        display: String){
            this.code = code;
            this.system = system;
            this.display = display;
        }
};