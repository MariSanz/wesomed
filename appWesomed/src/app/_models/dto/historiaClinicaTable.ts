export class HistoriaClinicaTable {
    id;
    date;
    practionerId;
    practionerFullName;
    status;
    result;

    constructor(date: Date, practionerId, practionerFullName, id, result: Array<JSON>, status?: String, ) {
        this.id = id;
        this.date = date;
        this.practionerId = practionerId;
        this.practionerFullName = practionerFullName;
        this.status = status;
        this.result = result;
    }
}