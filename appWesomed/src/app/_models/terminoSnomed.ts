export class TerminoSnomed {

    id: String;
    snomedid: String;
    prefLabel: String;
    definitionStatus: string;
    associatedWith : Array<TerminoSnomed>

    constructor(id: String,   snomedid : String,   prefLabel : String){
        this.id =  id;
        this.snomedid = snomedid;
        this.prefLabel = prefLabel;
       
        this.associatedWith = new Array();
    }

    addAssociatedWith(associated:TerminoSnomed){
       
        if(this.associatedWith != null){
            this.associatedWith.push(associated);
        }else{
            new Error("Array associatedWith - No inicializado");
        }
        
    }


}
