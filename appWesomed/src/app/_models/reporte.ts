import { Patient } from "./patient";

export class Reporte {  
    id;
    historyId : String;
    patient : Patient; 
    anamnesis: String;
    diagnostico: String;
    tratamiento: String;
    fecha: Date;
    
    constructor( patient : Patient, anamnesis: String, diagnostico: String,tratamiento:String, historyId: String ){
        this.patient = patient;
        this.anamnesis = anamnesis; 
        this.diagnostico = diagnostico;
        this.tratamiento = tratamiento;    
        this.historyId = historyId;   
    }
}
