import { User } from "./user";
import { LogLevelEnum } from "./_enum/logLevelEnum";

export class LoggerMessage {
    date:Date;
    text: String;
    level: String;
    source : String;

    constructor(textParam: String, levelParam: String, sourceParam : String) {
        this.date = new Date();
        this.text =  textParam;
        this.level = levelParam;
        this.source = sourceParam;
    }
}


export class Logger {

    user:String;
    message: LoggerMessage;
}