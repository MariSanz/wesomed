export class Person {
    identifier: string;
    name: string;
    surname: string;
    firstName: string;
    telecom: String;
    gender: string;
    age: number;
    birthdate: Date;

    constructor(identifier: string, name: string, surname: string, firstName: string,
        gender: string, age: number, birthdate: Date, telecom: String) {
        this.identifier = identifier;
        this.name = name;
        this.surname = surname;
        this.firstName = firstName;
        this.gender = gender;
        this.age = age;
        this.birthdate = birthdate;
        this.telecom = telecom;
    }
};