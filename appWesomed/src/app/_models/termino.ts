export class Termino {
    descriptionId: String;
    conceptId: String;
    term: string;
    active: boolean;
    conceptActive: boolean;
    definitionStatus: string;
    fsn: string;
    semanticTag: string;
    
}