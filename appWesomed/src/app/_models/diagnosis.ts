import { Condition, TerminoSnomed, Coding } from "./index";

export class Diagnosis {
    condition : Condition;

    createCondition(coding: Coding, snomedTerm: TerminoSnomed) {
        this.condition = new Condition(coding, snomedTerm);
    }

}
