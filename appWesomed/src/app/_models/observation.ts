import { Encounter , EncounterClassEnum } from "./index";

export class Observation {

    status: String;
    context: {
        encounter: Encounter
    };

    constructor(status: String, encounter: Encounter) {
        this.status = status;
        this.context = {
            encounter: encounter
        } ;
       
    }

    setEncounter(_class: EncounterClassEnum) {
        this.context = {
            encounter: new Encounter(_class)
        }
    }

}