import { Coding,  TerminoSnomed } from "./index";

export class Condition {
    coding: Coding;
    snomed_term: TerminoSnomed;

    constructor(coding: Coding, snomedTerm: TerminoSnomed) {
        this.coding = coding;
        this.snomed_term = snomedTerm;
    }
}