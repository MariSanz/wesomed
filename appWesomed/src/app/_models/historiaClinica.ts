import { Patient , Practitioner, Observation } from "./index";

export class HistoriaClinica {  
    id;
    date: Date;
    status: String;
    patient : Patient;   
    practitioner: Practitioner;
    result: Array< {
        observation : Observation;
    }>;
    
    constructor(date : Date, status: String, patient : Patient, practitioner? : Practitioner ){
       
        this.date = date;
        this.status = status;
        this.patient = patient;
        this.practitioner = practitioner; 
        this.result = new Array();         
    }

    addResult(observation : Observation){       
        this.result.push(  {
            observation : observation
        });
    }    
}