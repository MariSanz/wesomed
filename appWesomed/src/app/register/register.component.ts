import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, UserService } from '../_services/index';
import { AppComponent } from '../app.component';
import { LoggerService } from '../_services/logger.service';
import { Logger, LoggerMessage } from '../_models';
import { LogLevelEnum } from '../_models/_enum/logLevelEnum';

@Component({
    templateUrl: 'register.component.html'
})

export class RegisterComponent {
    model: any = {};
    log: Logger  = new Logger();
    loading = false;

    parent : AppComponent;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private loggerService : LoggerService,
        private inj:Injector
        ) { 
            this.parent = this.inj.get(AppComponent);
            this.parent.loadLog(this.log, 'Register');
        }

    register() {
       
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.log.message = new LoggerMessage('register -> success - ' + this.model.username, LogLevelEnum.TRACE, 'Register');
                    this.loggerService.create(this.log);
                    
                    this.router.navigate(['/login']);
                },
                exp => {
           
                    this.alertService.error('Usuario ya existe');
                    this.parent.handlerError(this.loggerService, this.log, exp, 'Register');
                    this.loading = false;
                });
    }
}
