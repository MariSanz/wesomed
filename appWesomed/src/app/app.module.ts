import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { routing } from './app.routing';

import { AppComponent } from './app.component';

//pipe
import { DateWithLocalePipe } from './_pipes/date-with-locale.pipe';

//compoments
import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptor, UtilList } from './_helpers/index';
import { AlertService, AuthenticationService, UserService, PatientService, HistoriaClinicaService, LoggerService, ReporteService } from './_services/index';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { PacienteComponent } from './paciente/index';
import { DatosPacienteDirectivaComponent } from './datos-paciente-directiva/datos-paciente-directiva.component';
import { DatosGeneralesDirectivaComponent } from './datos-generales-directiva/datos-generales-directiva.component';
import { AntHeredadosComponent } from './ant-heredados/ant-heredados.component';
import { AntNoPatologicosComponent } from './ant-no-patologicos/ant-no-patologicos.component';
import { HistoriaClinicaAltaComponent, HistoriaClinicaAltaAnamnesisComponent, HistoriaClinicaAltaDiagnosticoComponent, HistoriaClinicaAltaTratamientoComponent } from './historia-clinica-alta/index';

//componets primeng
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TabViewModule} from 'primeng/tabview';
import {TableModule} from 'primeng/table';
import {PanelModule} from 'primeng/panel';
import {CheckboxModule} from 'primeng/checkbox';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {MenubarModule} from 'primeng/menubar';
import {InputSwitchModule} from 'primeng/inputswitch';
import {RadioButtonModule} from 'primeng/radiobutton';
import {MultiSelectModule} from 'primeng/multiselect';
import {DialogModule} from 'primeng/dialog';
import {FieldsetModule} from 'primeng/fieldset';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    AlertComponent,
    RegisterComponent,
    PacienteComponent,
    DatosPacienteDirectivaComponent,
    DatosGeneralesDirectivaComponent,
    AntHeredadosComponent,
    AntNoPatologicosComponent,
    HistoriaClinicaAltaComponent,
    HistoriaClinicaAltaAnamnesisComponent,
    HistoriaClinicaAltaDiagnosticoComponent,
    HistoriaClinicaAltaTratamientoComponent,
    DateWithLocalePipe 
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    routing,
    InputTextModule,
    ButtonModule,
    TableModule,
    TabViewModule,
    PanelModule,
    CheckboxModule,
    ScrollPanelModule,
    MenubarModule,
    InputSwitchModule,
    RadioButtonModule,
    MultiSelectModule,
    DialogModule,
    FieldsetModule
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    PatientService,
    UtilList,
    UserService,
    ReporteService,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: JwtInterceptor,
        multi: true
    },
    HistoriaClinicaService,
    LoggerService,
    {provide: LOCALE_ID, useValue: 'en-EN' }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
