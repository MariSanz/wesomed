// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url_services_user: 'http://18.188.197.91:4000/',
  url_services: 'http://18.188.197.91:5000/',
  services: {
    user: 'wesomed/users/',
    authenticate: 'wesomed/users/authenticate',
    patient: 'wesomed/patients/',
    historia_clinica: 'wesomed/historias-clinicas/',
    log: 'wesomed/log/',
    reporte: 'wesomed/reporte/'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
