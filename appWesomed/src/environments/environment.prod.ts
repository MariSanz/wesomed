export const environment = {
  production: true,
  url_services_user: 'http://18.188.197.91:4000/',
  url_services: 'http://18.188.197.91:5000/',
  services: {
    user: 'wesomed/users/',
    authenticate: 'wesomed/users/authenticate',
    patient: 'wesomed/patients/',
    historia_clinica: 'wesomed/historias-clinicas/',
    log: 'wesomed/log/',
    reporte: 'wesomed/reporte/'
  }
};
