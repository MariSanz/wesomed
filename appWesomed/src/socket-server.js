var express = require('express');
var app = express();

var http = require('http');

var server = require('http').Server(app);

var io = require('socket.io')(server);

var me = this;


io.on('connection', function (socket) {
  console.log('Conexión al servidor correcta');
  //Emite mensaje de bienvenida al socket
  socket.emit('messages', 'Bienvenido, conexión correcta');
  //Escucha y emite 
  socket.on('new', function (data) {
     
   // console.log("Recibe " + data);

    var buffer = '';
   

    var url = 'http://localhost:3000/v2/snomed/snomed-term/v20140731/descriptions?query=' +
      data.term +
      '&searchMode=partialMatching&lang=spanish';


    var req = http.get(url,
      function (response) {
       
        if (req.res.statusCode == 200) {

          response.on('data', function (chunk) {

            buffer += chunk;
          });
          response.on('end', function () {

            var json = JSON.parse(buffer);
            var matches = json.matches;
           
            socket.emit('result', {
              dato: matches
            });
          });
        } else {
           
            console.error('Error en API');
        }
      });

    req.on('error', function (e) {
      console.log('Error en el procesado de respuesta');
      console.error(e);
    });

    req.on('timeout', function () {
      console.log('timeout');
      req.abort();
    });


    req.end();

  });


  socket.on('new-diagnostico', function (data) {
    socket.emit('result-diagnostico', {
      dato: data
    });
  });

  socket.on('new-tratamiento', function (data) {
    socket.emit('result-tratamiento', {
      dato: data
    });
  });
});

server.listen(88, function () {
  console.log('El servidor esta activo, puerto 88');
});
